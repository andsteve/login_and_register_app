'use strict';

const config = {
	appFolder: {
		app:'./app',
		filePaths: {
			folder_sass: 'app/assets/sass',
			folder_js: 'app/assets/js',
			sass: 'app/assets/sass/**/*.scss',
			main_sass: 'app/assets/sass/main.scss',
			js: 'app/assets/js/**/*.js',
			mainJs: 'app/assets/js/main.js',
			handlebars: 'app/views/**/*.handlebars',
			images: 'app/assets/images/**/*'
		},
	},
	buildFolder: {
		build:'./build',
		watchPaths: {
			css: './build/assets/css/**/*.css',
			js: 'build/assets/js/**/*.js',
			html: './build/views/**/*.html',
			handlebars: './build/views/**/*.handlebars',
			main_css: './build/assets/css/main.css'
		},
		destFolders: {
			css: './build/assets/css',
			main_css: 'build/assets/css/main.css',
			js: 'build/assets/js',
			handlebars: './build/views',
			images: 'build/assets/images' 
		},
	},
	express: {
		file:'app.js',
		route_folder: 'routes/*.js',
		model_folder: 'models/*.js'
	}
};

module.exports = config