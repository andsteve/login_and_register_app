'use strict';

// require
var gulp = require("gulp");
var minify = require('gulp-minify');

// config file
var config = require('../config');

/* this task minifies the main js file in one line of code */
gulp.task('minify_js', ['concat_js'],function() {
	gulp.src([config.buildFolder.watchPaths.js])
		.pipe(minify({
			ext:{
				src:'-debug.js',
				min:'.js'
			},
			exclude: ['tasks'],
			ignoreFiles: ['.combo.js', '-min.js','main-debug.js']
		}))
		.pipe(gulp.dest(config.buildFolder.destFolders.js))
});
