'use strict';

// requires
var gulp = require("gulp");
var concat = require("gulp-concat");

// config file
var config = require('../config');

/* this task concats all js files in one file */
gulp.task('concat_js', function() {
  return gulp.src([config.appFolder.filePaths.js])
    .pipe(concat('main.js'))
    .pipe(gulp.dest(config.buildFolder.destFolders.js));
});