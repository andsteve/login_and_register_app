var authenticated = {};

/* this funtion checks if the user is logged */
authenticated.ensureAuthenticated = function(req, res, next){
	
	/* checks if the user is logged */
	if(req.isAuthenticated()){
		
		return next();
	
	}else{
		
		/* redirect to login */
		res.redirect('/login');
	}
}

/* this funtion checks if the user is no logged */
authenticated.isNotAuthenticated = function(req, res, next){
	
	/* checks if the user is no logged */
	if(!req.isAuthenticated()){
		
		return next();
	}else{
		/* redirect to admin */
		res.redirect('/');
	}
}

module.exports = authenticated;