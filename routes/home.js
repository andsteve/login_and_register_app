'use strict';

//EXPRESS
var express = require('express');
var router = express.Router();
var multer  = require('multer');

//ENCRIPT
var bcrypt = require('bcrypt-nodejs');

//AUTHTENTICATE
var authenticated = require('./authenticated');

//MODELS
var UserModel = require('../models/user');

//PATHS CONGIF
var PATHS_CONFIG = require('./PATHS_CONFIG');

//PASSPORT
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

//ROUTES
/* home route */
router.get('/' , home );

/* register route */
router.get('/register', authenticated.isNotAuthenticated, register );

/* login route */
router.get('/login', authenticated.isNotAuthenticated, login );

/* Route to register new user */
router.post('/register', authenticated.isNotAuthenticated, registerNewUser);

/* Route to logout */
router.get('/logout', authenticated.ensureAuthenticated, logout);

/* Route to register new user */
router.post('/login',loginUser);

/* this function renter to home view */
function home(req,res){

	res.render('home');
};

/* this function renter to register view */
function register(req,res){

	res.render('register');
};

/* this function renter to login view */
function login(req,res){

	res.render('login');
};

/* this function logout the user */
function logout(req,res){

	req.logout();
	req.flash('success_msg','You are logged out !!');
	res.redirect('/login');
}

/* this function logins new user to the app */
function loginUser(req,res,next){

	// function to validates the input fields values 
	var errors = validateBody(req);
	var Errors = {};

	// checks if there are errors
	if(!errors){

		// checks if the email exist in the database
		findByEmail(req.body.email, function(err, user) {

			// checks if there are errors
			if(err){
				res.send({'errors': err});
			}

			// checks if exist user with that email
			if(!user){
				
				// send error status with the code 409
				Errors.data = {"emailPasswordError" : "The email or password is incorrect !!"};
				Errors.status = 409;

				res.send({'errors': Errors.data});
			}

			/* compares the password entered with the password of the user in the database */
			bcrypt.compare(req.body.password, user.password, function(err, equalsPassword) {

				// object with the user details
				var authUser = {
					'id' : user._id,
					'name' : user.name,
					'lastname' : user.lastname,
					'email':user.email,
					'avatar':user.avatar
				}

				// checks if both passwords are equals and if there are not errors
				if(equalsPassword && !err){
					
					// logins the user to the app
					req.logIn(authUser, function(err) {

						// send a success request
						res.send({'success': 200});
					});
				}else{

					// send error status with the code 409
					Errors.data = {"emailPasswordError" : "The email or password is incorrect !!"};
					Errors.status = 409;

					res.send({'errors': Errors.data});
				}
			});
		});
	}else{

		// send error status with the code 400
		Errors.data = errors;
		Errors.status = 400;
		res.send({'errors': Errors.data});
	}
}

/* function moves the image seleted to another path */
var storage = multer.diskStorage({

	/* destination where the image are going be saved */
	destination: function (req, file, callback) {

		// function to validates the input fields values 
		var errors = validateBody(req);
		var Errors = {};
		
		// checks if the inputs values have errors
		if(!errors){
		
			// model user to find user with his email
			UserModel.emailExist(req.body.email,function(err,user){

				// checks if the user email already exist in the database
				if(!user){

					callback(null, PATHS_CONFIG.images_folders.users);	
				}else{ 

					// send error status with the code 409
					Errors.data = {"emailExist" : "The email is already registered"};
					Errors.status = 409;

					callback(Errors);
				}
			});	
		}else{

			// send error status with the code 400
			Errors.data = errors;
			Errors.status = 400;

			callback(Errors);
		}
	},

	/* assigns a new name to the image file */
	filename: function (req, file, callback) {

		var extention = '.';
		var patternName = /\.([^.]*)$/;
		var patternExtention = /^.+\./;

		// gets only the extention of the original image name
		extention += file.originalname.replace(patternExtention,'');

		callback(null, req.body.name + '-' + new Date().toISOString() + extention);
	}
});


/* function validates the image file selected */
function fileFilter(req, file, callback) {

	// TRUE is the image file is valie FALSE if not
	var isValid = validateFileExtantion(file.mimetype);

	// checks if the image is valid
	if(isValid){

		callback(null, true);
	}else{

		// send error status with the code 400
		var Errors = {};
		Errors.data = {"FileExtention" : "The the file selected is not valid"};
		Errors.status = 400;

		callback(Errors);
	}
}

/* this funtions validates if the value of body does match with this regex */
function validateBody(req) {

	// gets error of the input value 
	var errors = checksPosiblesInputsNames(req);

	// checks is the are errors 
	if(errors){

		return errors;
	}

	return false;
}

/* funtion to review witch inputs comes in the body request and executes his own regex */
function checksPosiblesInputsNames(req){

	var errors = null;

	// REGEXS
	var emailRegex = /^(([\w])+(\.?))+(\@)(([a-z]{1,10})(((\.)([a-z])+)+)?)+(\.)([a-z]{2,3})$/;
	var textRegex = /(^[A-Za-z]{4,20})$/;
	var passwordRegex = /(?=.{8,})(?=.*[a-z])(?=.*[A-Z])()(?=.*[\W]).*$/;
	var filePatterRegex = /^([^.:](([\W]^[^:])*(\w)*)(\.)*)+((\.)((jpeg)|(jpg)|(png)))$/;

	// checks if the name comes of the body or if the name is empty 
	if(req.body.name || req.body.name === ''){
		req.checkBody('name', 'The name is required').notEmpty().doMatchRegex(textRegex,req.body.name).withMessage('The name does not have a correct format !');
	}

	// checks if the lastname comes of the body or if the lastname is empty 
	if(req.body.lastname || req.body.lastname === ''){
		req.checkBody('lastname', 'The last name is required').notEmpty().doMatchRegex(textRegex,req.body.lastname).withMessage('The lastname does not have a correct format !');	
	}

	// checks if the email comes of the body or if the email is empty 
	if(req.body.email || req.body.email === ''){
		req.checkBody('email', 'The email is required').notEmpty().doMatchRegex(emailRegex,req.body.email).withMessage('The email does not have a correct format !');	
	}

	// checks if the password comes of the body or if the password is empty 
	if(req.body.password || req.body.password === ''){
		req.checkBody('password', 'The password is required').notEmpty().doMatchRegex(passwordRegex,req.body.password).withMessage('The password does not have a correct format !');
	}

	// checks if the confirm_password comes of the body or if the confirm_password is empty 
	if(req.body.confirm_password || req.body.confirm_password === ''){
		req.checkBody('confirm_password', 'The confirm password is required').notEmpty().equals(req.body.password).withMessage('The confirm password does not match with password !');
	}

	// function returns the error of the inputs values
	var errors = req.validationErrors();	
	
	return errors;
}

/* function to validate the extention of the image file */
function validateFileExtantion(mimetype){

	var extentionsRegex = /^((image\/jpeg)|(image\/jpg)|(image\/png))$/;
	
	// TRUE if mimetype does match with extentionsRegex and FALSE if not
	var isValid = extentionsRegex.test(mimetype);

	return isValid;
}

/* moves the image selected to another path */
var upload = multer({ 
		storage: storage,
		fileFilter:fileFilter
	}).single("avatar");

/* this function registers new users */
function registerNewUser(req,res,next){

	// this function is executed to move the image file to the avatars user folder
	upload(req, res, function(err) {

		// checks if the user selected a image file and if the all data of the user is correct
		if(!err && req.file && req.body.avatar === undefined){

			// sets the body.avatar with the name of the file
			req.body.avatar = req.file.filename;

			// creates new model user and save to the database
			createUser();
			
		}else{
			var Errors = {};

			// checks if the user doesn't selected a image file and if all data of the user is correct
			if(!err && !req.file && req.body.avatar === ''){
				
				// this function validate the data of the user
				var errors = validateBody(req);
				
				// checks if there are not errors
				if(!errors){
				
					// model user to chacks if the email already exist
					UserModel.emailExist(req.body.email,function(err,user){

						// checks if the user email already exist in the database
						if(!user){

							// sets the body.avatar with the default image
							req.body.avatar = PATHS_CONFIG.images_folders.default_avatar;
							
							// creates new model user and save to the database
							createUser();
							
						}else{

							// send error status with the code 409
							Errors.data = {"emailExist" : "The email is already registered"};
							Errors.status = 409;

							res.send({'errors': Errors.data});
						}
					});	
				}else{

					// send error status with the code 400
					Errors.data = errors;
					Errors.status = 400;
					res.send({'errors': Errors.data});
				}
			}else{

				// send errors of the input data user
				res.send({'errors': err.data});
			}
		}

		/* funcion creates a model of the user and insert new user to the database */
		function createUser(){

			var newUser = new UserModel({
				avatar: req.body.avatar, 
				name: req.body.name, 
				lastname: req.body.lastname, 
				email: req.body.email,
				password: req.body.password
			});

			// insert a new user to the database
			UserModel.createUser(newUser,function(err,user){
				
				// checks if the user was inserted and if there are not errors
				if(!err && user){
					
					res.send({'success': 'You was uccessfully to the team!'});
				}else{

					res.send({'error': 'You the user can not be saved'});
				}
			});
		}	
	});
}

/* function finds the user by her/his email */
function findByEmail(email, callback){

	// Finds the user by her/his email
	UserModel.emailExist(email, function(err, user) {
		callback(err,user);	
	});
}

// passport configuration
passport.serializeUser(function(user, done) {
	done(null, user);
});

// passport configuration  
passport.deserializeUser(function(user, done) {
	done(null, user);
});

// return router;
module.exports = router;
