module.exports = {
	app_folder: './app',
	build_folder: './build',
	images_folders: {
		default_avatar:'assets/images/users/avatar.png',
		users: './build/assets/images/users',
	}
};