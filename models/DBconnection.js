'use strict';

// // REQUIRES
var mongoose = require('mongoose');

//MONGOOSE CONNECTION
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/DBusers',{ config: { autoIndex: false } });

module.exports = mongoose;