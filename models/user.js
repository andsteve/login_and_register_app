'use strict';

// REQUIRES
var bcrypt = require('bcrypt-nodejs');
var mongoose = require('./DBconnection');

// SCHEMA
var UserSchema = mongoose.Schema({

	'avatar':{
		type:String,
		required : true,
		dropDups: true
	},

	'name':{
		type:String,
		index:true,
		required : true,
	},
	'lastname':{
		type:String,
		index:true,
		required : true,
	},
	'email':{
		type:String,
		index:true,
		unique : true,
		required : true,
		dropDups: true
	},
	'password':{
		type:String,
		index:true,
		required : true,
	}
}, { autoIndex: false });

// MODEL
var User = mongoose.model('users', UserSchema);

/* this fucntion insert a new user to the users collection*/
User.createUser = function(newUser,callback){

	// encript password
	bcrypt.hash(newUser.password, null, null, function(err, hash) {

		// sets the password encrypted
		newUser.password = hash;

		// save the new user
		var user  = newUser.save();

		user.then(function (user) {

			// this is used to be sure that there isn't two email equlas on the database
			User.ensureIndexes();

			// ones the user is saved calls the callback and send the the new user 
			callback(null, user);

		}).catch(function(err){

			// if the there is and error, send the error details
			callback(err, null);
		});
	});
}

/* this function finds a user by his email */
User.emailExist = function(email,callback){

	User.findOne({"email":email},function(err, users){

		if(users){
			callback(null, users);
		}else{
			callback(err, null);
		}
		
	});
}

// export user model
module.exports = User;















