/**
 * Requires files
 */
 var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var expressValidator = require('express-validator');
var cookieParser = require('cookie-parser');
var exphbs = require('express-handlebars');
var sessions = require('express-session');
var bodyParser = require('body-parser');
var flash = require('connect-flash');

var express = require('express');
var http = require('http');
var path = require('path');

var app = module.exports = express();

/* bodyParser configuration */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

/* routes configuration */
var home = require('./routes/home');

/* handlebars configuration */
app.set('views', path.join(__dirname,'build/views/renders'));
var handlebars = require('express-handlebars').create({
	defaultLayout:'main',
	extname: '.handlebars',
	layoutsDir:'build/views/layouts',
	partialsDir:'build/views/partials'
});
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

/* set public directory */
app.use(express.static(__dirname + '/build'));

app.use(sessions({
	secret: 'secret',
	saveUninitialized: true,
	resave: true
}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());

// In this example, the formParam value is going to get morphed into form body format useful for printing.
app.use(expressValidator({
	errorFormatter: function(param, msg, value) {
		var namespace = param.split('.')
		, root = namespace.shift()
		, formParam = root;

		while(namespace.length) {
			formParam += '[' + namespace.shift() + ']';
		}
		return {
			param : formParam,
			msg   : msg,
			value : value
		};
	}
}));

app.use(expressValidator({
	customValidators: {
		doMatchRegex: function(value,regex) {
			return regex.test(value);
		}
	}
}));


/* allow flash messages */
app.use(flash());

// Global Variables
app.use(function (req, res, next) {
	res.locals.success_msg = req.flash('success_msg');
	res.locals.error_msg = req.flash('error_msg');
	res.locals.error = req.flash('error');
	res.locals.user = req.user || null;
	next();
});

/* files to render */
var models = [home];

app.use('/', models);

/* if the route is not found it render to*/
app.use(function (req, res, next) {

	res.render('404');
})

app.set('port', process.env.PORT || 3000);

/**
 * Start Server
 */
http.createServer(app).listen(app.get('port'), function () {
	console.log('Express server listening on port ' + app.get('port'));
});