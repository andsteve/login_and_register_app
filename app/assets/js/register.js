var APP = window.APP = window.APP || {};

APP.register = (function(){
	'use strict';

	// gets the container with the class register-container
	var registerContainer = document.getElementsByClassName('register-container')[0];

	// checks if the user is in the register container
	if(registerContainer){
		
		// gets the form register
		var formRegister = registerContainer.getElementsByClassName('form-register')[0];
	
		// gets the button register
		var btnRegister = formRegister.getElementsByClassName('btn-register')[0];

		// gets the button cancel
		var btnCancel = formRegister.getElementsByClassName('btn-cancel')[0];

		//gets all divs data-field
		var dataFields = formRegister.getElementsByClassName('data-field');

		//regular expresions to validate the inputs form
		var input_email_pattern = /^(([\w])+(\.?))+(\@)(([a-z]{1,10})(((\.)([a-z])+)+)?)+(\.)([a-z]{2,3})$/;
		var input_text_pattern = /(^[A-Za-z]{4,20})$/;
		var input_password_patter = /(?=.{8,})(?=.*[a-z])(?=.*[A-Z])()(?=.*[\W]).*$/;
		var input_file_patter = /^([^.:](([\W]^[^:])*(\w)*)(\.)*)+((\.)((jpeg)|(jpg)|(png)))$/;

		//add eventlistener to the btn-register
		btnRegister.addEventListener('click',valideteForm);
		
		//add eventlistener to the btn-cancel
		btnCancel.addEventListener('click',clearInputs);


		createsFocusAndBlurEvent();

	}

	function createsFocusAndBlurEvent(){
		
		// gets all inputs form
		var input_list = getInputs(dataFields);
		
		var input_name = '';
		var span_message = '';

		input_list.forEach(function(value, i){

			input_name = value.getAttribute('name');
			
			span_message = value.previousElementSibling;

			if(input_name === 'password' || input_name === 'confirm_password'){
			// 	console.log(input_name)

				(function (value,span_message) {
					
					value.addEventListener('focus', function() {
						span_message.innerHTML = "The password must have: numbers, letters, special characters, capital letters, greater than 8 <br/> Allow password format could be: 123abc%A \n ";
						span_message.classList.add("focus-message");
						span_message.classList.remove("input-message");
						span_message.classList.remove("warning");
					});
					value.addEventListener('blur', function() {
						span_message.innerHTML = "";
						span_message.classList.remove("focus-message")
						span_message.classList.add("input-message");
					});

				})(value,span_message);
			}
		});
	}

	/* function to validate inputs of the register form */
	function valideteForm (event){
		
		// gets all inputs form
		var input_list = getInputs(dataFields);
		
		// message to display
		var span_message = '';
		
		// number of inputs have been validated
		var count = 0;

		// FALSE if value of the input hasn't the format of the regex TRUE if has it
		var valid_pattern = false;
		
		// input name that is current checking 
		var input_name = '';

		// type of the current input
		var type = '';
				
		// value of the input
		var value = '';

		var data = {};
		
		//formdata to send files to the server side via ajax
		var formData = new FormData();
		
		// file selected
		var avatarFile = '';
		
		// checks each input form
		for(var i = 0; i < input_list.length; i++){

			// gets the current span on the current input 
			span_message = input_list[i].previousElementSibling;
	
			// gets the name of the current input
			input_name = input_list[i].getAttribute('name');

			// checks if the input value is null
			if(input_list[i].value === '' && input_name !== 'avatar'){			
				
				// sets message if the input is empty
				span_message.innerHTML = 'This field can not be empty';
				
				// add warning class to the current span
				span_message.classList.add('warning');

			}else{

				// add 1 if the input is not empty
				count++

				// removes the warning class if the input is not empty
				span_message.classList.remove('warning');
				
				// removes the message of the current span
				span_message.innerHTML = '';

				// gets type of the current input
				var type = input_list[i].getAttribute('type');
				
				// gets the value of the input
				var value = input_list[i].value;

				// gets FALSE if the value of the current input hasn't the format of the regex and TRUE if has it
				valid_pattern = validatInputsPattern(type,value);
				
				// checks the result of the valid_pattern
				if(valid_pattern === false){

					// subtract 1 if the valid_pattern is false
					count--;
					
					// sets message if the input hasn't valid format of the regex
					span_message.innerHTML = 'The ' + input_name + ' does not have a correct format !';
					
					// add warning class to the current span
					span_message.classList.add('warning');

				}else{

					// gets the value of the input 
					var value = addNewValuetoDataObject(formData,input_list[i],input_name)
					
					// checks if the value is not empty
					if(value){

						// sets the avatarfile with the value data
						avatarFile = value
					}

					// removes the warning class if the input has a valid format of the regex
					span_message.classList.remove('warning');
					
					// removes the message of the current span
					span_message.innerHTML = '';
				}
			}

			// checks if the input list is the last one, to be sure that is the confirm password input
			if( count === input_list.length){
				
				// checks if both passwords are not equals
				if(input_list[i].value !== input_list[i-1].value){
					
					// gets the name of the current input
					var confirmPassword = input_list[i].getAttribute('name');
					var password = input_list[i-1].getAttribute('name');
					
					// sets message if the input hasn't valid format of the regex
					span_message.innerHTML = 'The ' + confirmPassword + ' does not match with the '+ password +' !';
					
					// add warning class to the current span
					span_message.classList.add('warning');

				}else{

					// removes the warning class if the input has a valid format of the regex
					span_message.classList.remove('warning');
					
					// removes the message of the current span
					span_message.innerHTML = '';

					// appends the avatarFile to formData
					formData.append("avatar", avatarFile);
					
					// funtion to send the formData object to the server side via ajax
					sendData(formData);
				}	
			}
		}

		event.stopPropagation();
		event.preventDefault();
		return false;
	}

	/* function to send the data to the server side via ajax */
	function sendData(data){
		
		// instance of XMLHttpRequest
		var xhttp = new XMLHttpRequest();
		
		// url route
		var urlOrigin = window.location.origin;
		var url = urlOrigin+"/register";

		// listening when the state changes
		xhttp.onreadystatechange = function(result) {
			
			// checks when the request is done
			if (this.readyState === 4 && this.status === 200) {
				
				cleanSpanMessage();
				
				var result = JSON.parse(result.target.response);
				
				// checks if there are error form the server side
				if(result.errors){
					
					// checks if the error is email exist
					if(result.errors.emailExist){

						// gets all input of the form
						var input_list = getInputs(dataFields);

						// gets the span of the email field
						var span_message = input_list[3].previousElementSibling;

						// sets message if the input hasn't valid format of the regex
						span_message.innerHTML =  result.errors.emailExist;
						
						// add warning class to the current span
						span_message.classList.add('warning');

					}else{

						displayErrorFromServerSide(result.errors);
					}
					
				}else{

					window.location = "/login";
				}
			}
		};

		// opens the ajax request
		xhttp.open("POST",url,true);
		// sends the formData to the server side
		xhttp.send(data);
	}

	/* function to append the current input value to the formData */
	function addNewValuetoDataObject(formData,input_list,input_name){
		
		// by default the avatarFile is empty
		var avatarFile = '';
		
		switch(input_name) {
			
			case 'avatar':
				// if the the input name is avatar sets the avatarFile to que current input file
				avatarFile = input_list.files[0];
				break;

			case 'name':
				// appends the name to the formData
				formData.append('name', input_list.value);
				break;

			case 'lastname':
				// appends the lastname to the formData
				formData.append('lastname', input_list.value);
				break;

			case 'email':
				// appends the email to the formData
				formData.append('email', input_list.value);
				break;

			case 'password':
				// appends the password to the formData
				formData.append('password', input_list.value);
				break;

			case 'confirm_password':
				// appends the confirm_password to the formData
				formData.append('confirm_password', input_list.value);
				break;

			default:
				break;
		}

		return avatarFile;
	}

	/* function to clear all inputs to the from */
	function clearInputs(){
		
		// gets all inputs form
		var input_list = getInputs(dataFields);

		// message to display
		var span_message = '';
		
		// runs each input form
		for(var i = 0; i < input_list.length; i++){

			// gets the current span on the current input 
			span_message = input_list[i].previousElementSibling;

			// removes the warning class if the input is not empty
			span_message.classList.remove('warning');

			span_message.innerHTML = '';
			input_list[i].value = '';
		}
	}

	/* function to clean the span tag message */
	function cleanSpanMessage(){
		var input_list = getInputs(dataFields);
		var span_message = '';
		for (var i = 0; i < input_list.length; i++) {
			// gets the current span on the current input 
			span_message = input_list[i].previousElementSibling;

			// removes the warning class if the input is not empty
			span_message.classList.remove('warning');

			span_message.innerHTML = '';
		}
	}

	/* function gets all inputs form to be validated */
	function getInputs(parentElement){
		
		var input_list = [];
		for(var i = 0; i < parentElement.length; i++){
			input_list.push(parentElement[i].getElementsByClassName('field')[0]);

		}
		return input_list;
	}

	/* function to test if the value of the input does match with his input pattern */
	function validatInputsPattern(type,value){
		
		// FALSE if value of the input hasn't the format of the regex TRUE if has it
		var valid_pattern = false;
		
		// checks if the current input is a file type
		if(type === 'file'){
			
			valid_pattern = true;
			
			if(value !== ''){
				//gets the real name of the input field value
				var file_value = value.split("\\").reverse()[0];

				// gets FALSE if the value of the file hasn't the format of the file pattern regex and TRUE if has it
				valid_pattern = input_file_patter.test(file_value);
			}
		}

		// checks if the current input is a text type
		if(type === 'text'){

			// gets FALSE if the value of the current input hasn't the format of the text pattern regex and TRUE if has it
			valid_pattern = input_text_pattern.test(value);
		}

		// checks if the current input is a email type
		if(type === 'email'){

			// gets FALSE if the value of the current input hasn't the format of the email pattern regex and TRUE if has it
			valid_pattern = input_email_pattern.test(value);
		}

		// checks if the current input is a password type
		if(type === 'password'){

			// gets FALSE if the value of the current input hasn't the format of the password pattern regex and TRUE if has it
			valid_pattern = input_password_patter.test(value);
		}

		return valid_pattern;
	}

	/* funtion to display all error from the server side */
	function displayErrorFromServerSide(errors){
		var input_list = getInputs(dataFields);
		var span_message = '';

		for(var i = 0; i < input_list.length; i++){
			span_message = input_list[i].previousElementSibling;
			span_message.classList.remove('warning');
			span_message.innerHTML = '';
			
			for (var c = 0; c < errors.length; c++) {

				// checks is the error.param does match with the name of the input
				if(errors[c].param === input_list[i].name){
					
					// sets message if the input hasn't valid format of the regex
					span_message.innerHTML =  errors[c].msg;
					
					// add warning class to the current span
					span_message.classList.add('warning');

					break;
				}
			}	
		}
	}
	return{
		methodOne : ''
	};
})();