var APP = window.APP = window.APP || {};

APP.main = (function(){
	'use strict';

	var navMenu = document.getElementsByClassName("nav-menu")[0];
	
	if(navMenu){
		var btnContainer = navMenu.getElementsByClassName("btn-container")[0];

		var smallBtnMenu = btnContainer.getElementsByTagName('button')[0];

		smallBtnMenu.addEventListener('click',function(){
			var ul = navMenu.getElementsByTagName('ul')[0];
			ul.classList.toggle("displayTopDown");
		});
	}

	return {
		
	}

})();






