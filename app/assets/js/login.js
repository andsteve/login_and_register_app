var APP = window.APP = window.APP || {};

APP.login = (function(){
	'use strict';

	// gets the container with the class login-container
	var loginContainer = document.getElementsByClassName('login-container')[0];

	// checks if the user is in the login container
	if(loginContainer){

		// gets the form login
		var formLogin = loginContainer.getElementsByClassName('form-login')[0];
	
		// gets the button login
		var btnLogin = formLogin.getElementsByClassName('btn-login')[0];

		// gets the button cancel
		var btnCancel = formLogin.getElementsByClassName('btn-cancel')[0];

		//gets all divs data-field
		var dataFields = formLogin.getElementsByClassName('data-field');

		//regular expresions to validate the inputs form
		var input_email_pattern = /^(([\w])+(\.?))+(\@)(([a-z]{1,10})(((\.)([a-z])+)+)?)+(\.)([a-z]{2,3})$/;
		var input_password_patter = /(?=.{8,})(?=.*[a-z])(?=.*[A-Z])()(?=.*[\W]).*$/;

		//add eventlistener to the btn-register
		btnLogin.addEventListener('click',valideteForm);

		//add eventlistener to the btn-cancel
		btnCancel.addEventListener('click',clearInputs);

		createsFocusAndBlurEvent();	
	}

	function createsFocusAndBlurEvent(){
		
		// gets all inputs form
		var input_list = getInputs(dataFields);
		
		var input_name = '';
		var span_message = '';

		input_list.forEach(function(value, i){

			input_name = value.getAttribute('name');
			
			span_message = value.previousElementSibling;

			if(input_name === 'password'){
			// 	console.log(input_name)

				(function (value,span_message) {
					
					value.addEventListener('focus', function() {
						span_message.innerHTML = "The password must have: numbers, letters, special characters, capital letters, greater than 8 <br/> Allow password format could be: 123abc%A \n ";
						span_message.classList.add("focus-message");
						span_message.classList.remove("input-message");
						span_message.classList.remove("warning");
					});
					value.addEventListener('blur', function() {
						span_message.innerHTML = "";
						span_message.classList.remove("focus-message")
					});

				})(value,span_message);
			}
		});
	}

	/* function to validate inputs of the login form */
	function valideteForm(){

		// gets the data object with values
		var data = getDataObject();

		// funtion to send the data object to the server side via ajax
		sendData(data);
	}

	/* function to load all inputs value of the form to the data object */
	function getDataObject(){
		var data = {};
		var input_name = '';

		// gets all inputs form
		var inputs = getInputs();
		
		for (var i = 0; i < inputs.length; i++) {
			
			// gets the name of the current input
			input_name = inputs[i].getAttribute('name');

			// add new value to the data object
			data = addNewValuetoDataObject(data,inputs[i],input_name);
		}

		return data;
	}

	/* function to append the current input value to the data object */
	function addNewValuetoDataObject(data,input,input_name){
		
		switch(input_name) {

			case 'email':
				// appends the email to the data objec
				data.email = input.value;
				break;

			case 'password':
				// appends the password to the data objec
				data.password = input.value;
				break;

			default:
				break;
		}

		return data;
	}

	/* function gets all inputs form */
	function getInputs(){
		var inputs = []

		for (var i = 0; i < dataFields.length; i++) {
			inputs.push(dataFields[i].getElementsByClassName("field")[0]);
		}

		return inputs
	}

	/* function to send the data to the server side via ajax */
	function sendData(data){

		var httpRequest = null;

		// url route
		var urlOrigin = window.location.origin;
		var url = urlOrigin+"/login";
		data = JSON.stringify(data);

		// checks compatibility
		if(window.XMLHttpRequest){

			// new instance of XMLHttpRequest()
			httpRequest = new XMLHttpRequest();

		}else if(window.ActiveXObject){

			// new instance of ActiveXObject()
			httpRequest = new new ActiveXObject("Microsoft.XMLHTTP");
		}

		// checks httpRequest is not null
		if(httpRequest){

			// listening when the state changes
			httpRequest.onreadystatechange = function(result){
				
				// checks when the request is done
				if(this.readyState === 4 && this.status === 200){

					// gets div to display message errors
					var divMessage = formLogin.getElementsByClassName('div-message')[0];
					// cleans div message with empty value
					divMessage.innerHTML = '';
					// remove warming class form div message
					divMessage.classList.remove('warning');

					// parse to an object the result of the request
					var result = JSON.parse(result.target.response);

					// checks if the result have errors
					if(result.errors){

						// checks if the error is a email and password error
						if(result.errors.emailPasswordError){

							// sets the div message with the error value
							divMessage.innerHTML = result.errors.emailPasswordError;
							// add warning class to div message
							divMessage.classList.add('warning');

						}else{

							// display error from the server side
							displayErrorFromServerSide(result.errors);
						}
					}else{

						// checks if the result is a success request
						if(result.success){
							
							// redirect to the home view
							window.location = urlOrigin;
						}
					}
				}
			};

			// opens the ajax request
			httpRequest.open("POST",url,true);

			// sets the Content-Type
			httpRequest.setRequestHeader('Content-Type', 'application/json');

			// sends the formData to the server side
			httpRequest.send(data);
		}
	}

	/* funtion to display all error from the server side */
	function displayErrorFromServerSide(errors){
		var input_list = getInputs(dataFields);
		var span_message = '';

		for(var i = 0; i < input_list.length; i++){
			span_message = input_list[i].previousElementSibling;
			span_message.classList.remove('warning');
			span_message.innerHTML = '';
			
			for (var c = 0; c < errors.length; c++) {

				// checks is the error.param does match with the name of the input
				if(errors[c].param === input_list[i].name){
					
					// sets message if the input hasn't valid format of the regex
					span_message.innerHTML =  errors[c].msg;
					
					// add warning class to the current span
					span_message.classList.add('warning');

					break;
				}
			}	
		}
	}	

	/* function to clear all inputs to the from */
	function clearInputs(){
		
		// gets all inputs form
		var input_list = getInputs(dataFields);

		// message to display
		var span_message = '';
		
		// runs each input form
		for(var i = 0; i < input_list.length; i++){

			// gets the current span on the current input 
			span_message = input_list[i].previousElementSibling;

			// removes the warning class if the input is not empty
			span_message.classList.remove('warning');

			// removes message of the span tag
			span_message.innerHTML = '';

			// cleans input of the form 
			input_list[i].value = '';
		}
	}

	/* function to clean the span tag message */
	function cleanSpanMessage(){
		var input_list = getInputs(dataFields);
		var span_message = '';
		for (var i = 0; i < input_list.length; i++) {
			// gets the current span on the current input 
			span_message = input_list[i].previousElementSibling;

			// removes the warning class if the input is not empty
			span_message.classList.remove('warning');

			span_message.innerHTML = '';
		}
	}	

	return {
		// methodOne : methodOne
	}
})();